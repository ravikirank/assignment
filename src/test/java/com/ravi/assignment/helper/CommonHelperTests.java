package com.ravi.assignment.helper;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ravi.assignment.AssignmentApplicationTests;

public class CommonHelperTests extends AssignmentApplicationTests {

	@Autowired
	private CommonHelper commonHelper;

	private String foodItem = null;
	private String[] foodItemTokens = new String[2];
	private String delimiterValid = " ";
	private String delimiterInValid = "*";

	@Before
	public void setup() {
		foodItem = "1093483 9387";
		foodItemTokens[0] = "1093483";
		foodItemTokens[1] = "9387";
	}

	/**
	 * Test for Null
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSplitStringForNull() throws Exception {
		String[] actual = commonHelper.splitString(anyString(), eq(null));
		assertNull(actual);
	}

	/**
	 * Test for proper result
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSplitString() throws Exception {
		String[] actual = commonHelper.splitString(delimiterValid, foodItem);
		assertArrayEquals(foodItemTokens, actual);
	}

	/**
	 * Test for throwing Exception
	 * 
	 * @throws Exception
	 */
	@Test(expected = Exception.class)
	public void testSplitStringException() throws Exception {
		String[] actual = commonHelper.splitString(delimiterInValid, foodItem);
		assertArrayEquals(foodItemTokens, actual);
	}
}
