package com.ravi.assignment.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;

import com.ravi.assignment.AssignmentApplicationTests;
import com.ravi.assignment.model.FoodItem;

public class FoodHelperTests extends AssignmentApplicationTests {

	@Value("${default.test.data.file.path}")
	private String defaultTestDataFilePath;

	@Autowired
	private FoodHelper foodHelper;

	@Mock
	private CommonHelper commonHelper;
	private String[] FOOD_ITEM_TOKENS = new String[2];
	private Map<String, Object> fileContents = null;
	private FoodItem foodItem = null;
	private List<FoodItem> foodItems = null;

	@Before
	public void setup() {
		FOOD_ITEM_TOKENS[0] = "1093483";
		FOOD_ITEM_TOKENS[1] = "9387";

		foodItem = new FoodItem(1093483, 9387);
		foodItems = new ArrayList<>();
		foodItems.add(foodItem);

		fileContents = new HashMap<>();
		fileContents.put("totalTime", 100);
		fileContents.put("noOfFoodItems", 1);
		fileContents.put("foodItems", foodItems);

		try {
			Mockito.when(commonHelper.splitString(anyString(), any())).thenReturn(FOOD_ITEM_TOKENS);
		} catch (Exception e) {
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testReadFile() throws FileNotFoundException, IOException, Exception {
		Map<String, Object> fileContents = foodHelper
				.parseDataFile(new ClassPathResource(defaultTestDataFilePath).getFile().getAbsolutePath());
		List<FoodItem> actualFoodItems = (List<FoodItem>) fileContents.get("foodItems"); // Use ObjectMapper
		FoodItem actualFoodItem = actualFoodItems.get(0);

		assertTrue(actualFoodItem.getSatisfaction().equals(foodItem.getSatisfaction()));
		assertTrue(actualFoodItem.getTimeTaken().equals(foodItem.getTimeTaken()));
	}

	@Test(expected = FileNotFoundException.class)
	public void testFileNotFound() throws FileNotFoundException, IOException, Exception {
		Map<String, Object> fileContents = foodHelper.parseDataFile(defaultTestDataFilePath);
	}

	@Test
	public void testGetMaximumSatisfaction() throws Exception {
		List<Integer> allSatisfactions = Arrays.asList(60, 100, 120);
		List<Integer> allTimes = Arrays.asList(10, 20, 30);
		int totalTime = 50;
		int noOfFoodItems = 3;
		int expected = 220;

		Map<String, Object> fileContents = new HashMap<>();
		fileContents.put("totalTime", totalTime);
		fileContents.put("noOfFoodItems", noOfFoodItems);
		fileContents.put("foodItems", foodItems);
		fileContents.put("allSatisfactions", allSatisfactions);
		fileContents.put("allTimes", allTimes);

		int actual = foodHelper.getMaximumSatisfaction(fileContents);
		assertEquals(expected, actual);
	}

	@Test(expected = ClassCastException.class)
	public void testGetMaximumSatisfactionCastException() throws Exception {
		int allSatisfactions = 100;
		List<Integer> allTimes = Arrays.asList(10, 20, 30);
		int totalTime = 50;
		int noOfFoodItems = 3;

		Map<String, Object> fileContents = new HashMap<>();
		fileContents.put("totalTime", totalTime);
		fileContents.put("noOfFoodItems", noOfFoodItems);
		fileContents.put("foodItems", foodItems);
		fileContents.put("allSatisfactions", allSatisfactions);
		fileContents.put("allTimes", allTimes);
		
		foodHelper.getMaximumSatisfaction(fileContents);
	}
}
