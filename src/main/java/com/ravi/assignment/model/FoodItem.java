package com.ravi.assignment.model;

import java.io.Serializable;

public class FoodItem implements Serializable {
	private static final long serialVersionUID = -1815889504147614441L;

	Integer satisfaction;
	Integer timeTaken;

	public FoodItem(Integer satisfaction, Integer timeTaken) {
		this.satisfaction = satisfaction;
		this.timeTaken = timeTaken;
	}

	public Integer getSatisfaction() {
		return this.satisfaction;
	}

	public void setSatisfaction(Integer satisfaction) {
		this.satisfaction = satisfaction;
	}

	public Integer getTimeTaken() {
		return this.timeTaken;
	}

	public void setTimeTaken(Integer timeTaken) {
		this.timeTaken = timeTaken;
	}
}
