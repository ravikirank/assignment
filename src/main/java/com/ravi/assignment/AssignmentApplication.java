package com.ravi.assignment;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ravi.assignment.helper.FoodHelper;
import com.ravi.assignment.model.FoodItem;

/**
 * Spring Boot Application
 * 
 * @author ravi
 * @version 1.0
 */
@SpringBootApplication
public class AssignmentApplication implements CommandLineRunner {

	@Autowired
	private FoodHelper foodHelper;

	public static void main(String[] args) {
		SpringApplication.run(AssignmentApplication.class, args);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run(String... args) throws Exception {
		/**
		 * Initialize
		 */
		String filePath = null;
		Map<String, Object> fileContents = null;
		int maximumSatisfaction = 0;
		List<FoodItem> foodItems = null;

		try {

			/**
			 * Check if FilePath is passed
			 */
			if (args != null && args.length > 1) {
				filePath = args[0];
			}

			/**
			 * Parse File
			 */
			fileContents = foodHelper.parseDataFile(filePath);

			/**
			 * Find Maximum Satisfaction Value
			 */
			maximumSatisfaction = foodHelper.getMaximumSatisfaction(fileContents);

			System.out.println("Maximum Satisfaction is : " + maximumSatisfaction);
		} catch (FileNotFoundException e) {
			System.out.println("Unable to locate data file.");
		} catch (ClassCastException e) {
			System.out.println("Unexpected data present in file. Please correct it before proceeding.");
		} catch (IOException e) {
			System.out.println("There is an error while parsing the file. Please try again.");
		} catch (Exception e) {
			System.out.println("There is an error while working with the file. Please try again.");
		}
	}
}
