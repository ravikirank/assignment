package com.ravi.assignment.helper;

import com.ravi.assignment.model.FoodItem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;

/**
 * Helper class for parsing the Food Items and finding out maximum satisfaction
 * 
 * @author ravi
 * @version 1.0
 */
@Component
public class FoodHelper {

	@Value("${default.data.file.path}")
	private String defaultDataFilePath;

	@Autowired
	private CommonHelper commonHelper;

	/**
	 * Parse File
	 * 
	 * @param filePath
	 * @return Map<String, Object> Total Time and Satisfaction/TimeTaken List for
	 *         each Item
	 * @throws IOException
	 */
	public Map<String, Object> parseDataFile(String filePath) throws FileNotFoundException, IOException, Exception {

		/**
		 * Initialize
		 */
		Map<String, Object> fileContents = null;
		InputStreamReader inputStream = null;
		BufferedReader reader = null;
		File dataFile = null;
		List<FoodItem> foodItems = new ArrayList<>();
		String currentLine = null;
		String[] tokens = null;
		int totalTime = 0;
		int noOfFoodItems = 0;
		String delimiter = " ";
		List<Integer> allSatisfactions = new ArrayList<>();
		List<Integer> allTimes = new ArrayList<>();
		int satisfaction = 0;
		int timeTaken = 0;

		try {

			if (filePath == null || filePath.isEmpty()) {
				filePath = defaultDataFilePath;
				dataFile = new ClassPathResource(filePath).getFile();
			} else {
				dataFile = new File(filePath);
			}

			/**
			 * Parse File
			 */
			inputStream = new InputStreamReader(new FileInputStream(dataFile));
			reader = new BufferedReader(inputStream);

			/**
			 * Read First Line
			 */
			currentLine = reader.readLine();
			if (currentLine != null && !currentLine.isEmpty()) {
				tokens = commonHelper.splitString(delimiter, currentLine);
				if (tokens != null && tokens.length == 2) {
					totalTime = Integer.parseInt(tokens[0]);
					noOfFoodItems = Integer.parseInt(tokens[1]);
				}
			}

			/**
			 * Read subsequent Food Item data
			 */
			while ((currentLine = reader.readLine()) != null && !currentLine.isEmpty()) {
				tokens = commonHelper.splitString(delimiter, currentLine);

				if (tokens != null && tokens.length == 2) {
					satisfaction = Integer.parseInt(tokens[0]);
					timeTaken = Integer.parseInt(tokens[1]);

					foodItems.add(new FoodItem(satisfaction, timeTaken));
					allSatisfactions.add(satisfaction);
					allTimes.add(timeTaken);
				}
			}

			fileContents = new HashMap<>();
			fileContents.put("totalTime", totalTime);
			fileContents.put("noOfFoodItems", noOfFoodItems);
			fileContents.put("foodItems", foodItems);
			fileContents.put("allSatisfactions", allSatisfactions);
			fileContents.put("allTimes", allTimes);

		} catch (FileNotFoundException e) {
			throw new FileNotFoundException();
		} catch (IOException e) {
			throw new IOException(e);
		} catch (Exception e) {
			throw new Exception(e);
		} finally {

			if (reader != null) {
				reader.close();
			}

			if (inputStream != null) {
				inputStream.close();
			}
		}

		return fileContents;
	}

	/**
	 * Sort Food Items based upon timeTaken
	 * 
	 * @param foodItems
	 * @return List<FoodItem> - Sorted Food Items
	 * @throws Exception
	 */
	public List<FoodItem> sortByTime(List<FoodItem> foodItems) throws Exception {
		try {
			foodItems.sort((f1, f2) -> f1.getTimeTaken().compareTo(f2.getTimeTaken()));
		} catch (Exception e) {
			System.out.println("sortByTime - Error while sorting");
			throw new Exception(e);
		}

		return foodItems;
	}

	/**
	 * Calculate Maximum Satisfaction
	 * 
	 * @param fileContents
	 * @return int - maximum satisfaction
	 */
	@SuppressWarnings("unchecked")
	public int getMaximumSatisfaction(Map<String, Object> fileContents) throws Exception {
		/**
		 * Declare/Initialize
		 */
		int maxSatisfaction = 0;
		int totalTime = 0;
		int noOfItems = 0;
		List<Integer> allSatisfactions = null;
		List<Integer> allTimes = null;
		int s, t;
		int[][] K = null;

		try {
			/**
			 * Assign
			 */
			totalTime = (int) fileContents.get("totalTime"); // Use ObjectMapper
			noOfItems = (int) fileContents.get("noOfFoodItems"); // Use ObjectMapper
			allSatisfactions = (List<Integer>) fileContents.get("allSatisfactions"); // Use ObjectMapper
			allTimes = (List<Integer>) fileContents.get("allTimes"); // Use ObjectMapper
			K = new int[noOfItems + 1][totalTime + 1];

			/**
			 * Fill the 2D matrix row by row
			 */
			for (s = 0; s <= noOfItems; s++) {
				for (t = 0; t <= totalTime; t++) {
					if (s == 0 || t == 0) {
						K[s][t] = 0;
					}
					/**
					 * Check if current item's timeTaken is <= running item's timeTaken
					 */
					else if (allTimes.get(s - 1) <= t) {
						/**
						 * Check if satisfaction of current item + satisfaction of the item that we
						 * could afford with the remaining timeTaken is greater than the satisfaction
						 * without the current item itself
						 */
						K[s][t] = Math.max(allSatisfactions.get(s - 1) + K[s - 1][t - allTimes.get(s - 1)],
								K[s - 1][t]);
					} else {
						/**
						 * If the current item's timeTaken is more than the running item's timeTaken,
						 * just carry forward the value without the current item
						 */
						K[s][t] = K[s - 1][t];
					}
				}
			}

			maxSatisfaction = K[noOfItems][totalTime];
		} catch (ClassCastException e) {
			throw new ClassCastException();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {

		}

		return maxSatisfaction;
	}
}
