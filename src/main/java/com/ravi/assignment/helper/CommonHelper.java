
package com.ravi.assignment.helper;

import org.springframework.stereotype.Component;

/**
 * 
 * Common Helper class
 * 
 * @author ravi
 * @version 1.0
 */
@Component
public class CommonHelper {

	/**
	 * Splits a string based upon given delimiter
	 * 
	 * @param delimiter
	 * @param data
	 * @return String[]
	 * @throws Exception
	 */
	public String[] splitString(String delimiter, String data) throws Exception {
		/**
		 * Initialize
		 */
		String[] tokens = null;

		try {

			if (delimiter != null && data != null) {
				tokens = data.split(delimiter);
			}

		} catch (Exception e) {
			throw new Exception(e);
		}

		return tokens;
	}
}
